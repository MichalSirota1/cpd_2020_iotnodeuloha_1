package sk.fri.uniza;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;
import sk.fri.uniza.api.WeatherStationService;
import sk.fri.uniza.model.WeatherData;

import java.io.IOException;
import java.util.List;

public class IotNode {
    private final Retrofit retrofit;
    private final WeatherStationService weatherStationService;

    public IotNode() {

        retrofit = new Retrofit.Builder()
                // Url adresa kde je umietnená WeatherStation služba
                .baseUrl("http://localhost:9000/")
                // Na konvertovanie JSON objektu na java POJO použijeme
                // Jackson knižnicu
                .addConverterFactory(JacksonConverterFactory.create())
                .build();
        // Vytvorenie inštancie komunikačného rozhrania
        weatherStationService = retrofit.create(WeatherStationService.class);

    }

    public WeatherStationService getWeatherStationService() {
        return weatherStationService;
    }

    public double getAverageTemperature(String station,String from, String to){
        double averageTemp = 0;
        Call<List<WeatherData>> currentWeathers = weatherStationService.getHistoryWeather(station, from, to, List.of("airTemperature"));
        try {
            // Odoslanie požiadavky na server pomocou REST rozhranie
            Response<List<WeatherData>> response = currentWeathers.execute();

            if (response.isSuccessful()) { // Dotaz na server bol neúspešný
                //Získanie údajov vo forme inštancie triedy WeatherData
                List<WeatherData> weathers = response.body();
                averageTemp = weathers.stream().map(x -> x.getAirTemperature()).reduce(0.0, (a,b)-> a+b);
                //System.out.println(averageTemp / weathers.size());
                /*averageTemp = 0;
                for (WeatherData temp : weathers ) {
                    averageTemp += temp.getAirTemperature();
                    System.out.println(temp.getAirTemperature());
                }*/
                return averageTemp / weathers.size();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        return -1000; //chybna nerealna teplota
    }
}
